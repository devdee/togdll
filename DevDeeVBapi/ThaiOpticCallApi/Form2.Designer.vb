﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtGetOMAByOrderID = New System.Windows.Forms.Button()
        Me.Txtpassword = New System.Windows.Forms.TextBox()
        Me.Txtuser = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BtGetOMAByRefNo = New System.Windows.Forms.Button()
        Me.BtGetOrderDefinition = New System.Windows.Forms.Button()
        Me.BtGetOrderDefinitions = New System.Windows.Forms.Button()
        Me.BtGetOrderIdentifiersByCustomerOrderNo = New System.Windows.Forms.Button()
        Me.BtGetOrderIDs = New System.Windows.Forms.Button()
        Me.BtGetUncollectedOrders = New System.Windows.Forms.Button()
        Me.BtSetOrderStatus = New System.Windows.Forms.Button()
        Me.BtSetOrderStatusByRefno = New System.Windows.Forms.Button()
        Me.BtSetOrderStatusWithETA = New System.Windows.Forms.Button()
        Me.BtSetOrderToInterventionStatus = New System.Windows.Forms.Button()
        Me.BtSetOrderToInterventionStatusByRefNo = New System.Windows.Forms.Button()
        Me.BtSetStatusCredit = New System.Windows.Forms.Button()
        Me.Txtmessage = New System.Windows.Forms.TextBox()
        Me.BtGetOrderIDByRefno = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtGetOrderIDByRefno)
        Me.GroupBox1.Controls.Add(Me.BtGetOrderIdentifiersByCustomerOrderNo)
        Me.GroupBox1.Controls.Add(Me.BtSetStatusCredit)
        Me.GroupBox1.Controls.Add(Me.BtGetOMAByOrderID)
        Me.GroupBox1.Controls.Add(Me.BtSetOrderToInterventionStatusByRefNo)
        Me.GroupBox1.Controls.Add(Me.BtSetOrderToInterventionStatus)
        Me.GroupBox1.Controls.Add(Me.BtSetOrderStatusWithETA)
        Me.GroupBox1.Controls.Add(Me.BtSetOrderStatusByRefno)
        Me.GroupBox1.Controls.Add(Me.BtSetOrderStatus)
        Me.GroupBox1.Controls.Add(Me.BtGetUncollectedOrders)
        Me.GroupBox1.Controls.Add(Me.BtGetOrderIDs)
        Me.GroupBox1.Controls.Add(Me.BtGetOrderDefinitions)
        Me.GroupBox1.Controls.Add(Me.BtGetOrderDefinition)
        Me.GroupBox1.Controls.Add(Me.BtGetOMAByRefNo)
        Me.GroupBox1.Controls.Add(Me.Txtpassword)
        Me.GroupBox1.Controls.Add(Me.Txtuser)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(628, 389)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Call"
        '
        'BtGetOMAByOrderID
        '
        Me.BtGetOMAByOrderID.Location = New System.Drawing.Point(342, 187)
        Me.BtGetOMAByOrderID.Name = "BtGetOMAByOrderID"
        Me.BtGetOMAByOrderID.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOMAByOrderID.TabIndex = 4
        Me.BtGetOMAByOrderID.Text = "GetOMAByOrderID"
        Me.BtGetOMAByOrderID.UseVisualStyleBackColor = True
        '
        'Txtpassword
        '
        Me.Txtpassword.Location = New System.Drawing.Point(342, 60)
        Me.Txtpassword.Name = "Txtpassword"
        Me.Txtpassword.Size = New System.Drawing.Size(280, 22)
        Me.Txtpassword.TabIndex = 3
        '
        'Txtuser
        '
        Me.Txtuser.Location = New System.Drawing.Point(6, 60)
        Me.Txtuser.Name = "Txtuser"
        Me.Txtuser.Size = New System.Drawing.Size(280, 22)
        Me.Txtuser.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(339, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 17)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "PASSWORD"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "USER NAME"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Txtmessage)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 407)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(628, 152)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Result"
        '
        'BtGetOMAByRefNo
        '
        Me.BtGetOMAByRefNo.Location = New System.Drawing.Point(342, 261)
        Me.BtGetOMAByRefNo.Name = "BtGetOMAByRefNo"
        Me.BtGetOMAByRefNo.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOMAByRefNo.TabIndex = 5
        Me.BtGetOMAByRefNo.Text = "GetOMAByRefNo"
        Me.BtGetOMAByRefNo.UseVisualStyleBackColor = True
        '
        'BtGetOrderDefinition
        '
        Me.BtGetOrderDefinition.Location = New System.Drawing.Point(6, 150)
        Me.BtGetOrderDefinition.Name = "BtGetOrderDefinition"
        Me.BtGetOrderDefinition.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOrderDefinition.TabIndex = 6
        Me.BtGetOrderDefinition.Text = "GetOrderDefinition"
        Me.BtGetOrderDefinition.UseVisualStyleBackColor = True
        '
        'BtGetOrderDefinitions
        '
        Me.BtGetOrderDefinitions.Location = New System.Drawing.Point(342, 150)
        Me.BtGetOrderDefinitions.Name = "BtGetOrderDefinitions"
        Me.BtGetOrderDefinitions.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOrderDefinitions.TabIndex = 7
        Me.BtGetOrderDefinitions.Text = "GetOrderDefinitions"
        Me.BtGetOrderDefinitions.UseVisualStyleBackColor = True
        '
        'BtGetOrderIdentifiersByCustomerOrderNo
        '
        Me.BtGetOrderIdentifiersByCustomerOrderNo.Location = New System.Drawing.Point(342, 298)
        Me.BtGetOrderIdentifiersByCustomerOrderNo.Name = "BtGetOrderIdentifiersByCustomerOrderNo"
        Me.BtGetOrderIdentifiersByCustomerOrderNo.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOrderIdentifiersByCustomerOrderNo.TabIndex = 8
        Me.BtGetOrderIdentifiersByCustomerOrderNo.Text = "GetOrderIdentifiersByCustomerOrderNo"
        Me.BtGetOrderIdentifiersByCustomerOrderNo.UseVisualStyleBackColor = True
        '
        'BtGetOrderIDs
        '
        Me.BtGetOrderIDs.Location = New System.Drawing.Point(6, 113)
        Me.BtGetOrderIDs.Name = "BtGetOrderIDs"
        Me.BtGetOrderIDs.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOrderIDs.TabIndex = 9
        Me.BtGetOrderIDs.Text = "GetOrderIDs"
        Me.BtGetOrderIDs.UseVisualStyleBackColor = True
        '
        'BtGetUncollectedOrders
        '
        Me.BtGetUncollectedOrders.Location = New System.Drawing.Point(342, 113)
        Me.BtGetUncollectedOrders.Name = "BtGetUncollectedOrders"
        Me.BtGetUncollectedOrders.Size = New System.Drawing.Size(280, 31)
        Me.BtGetUncollectedOrders.TabIndex = 10
        Me.BtGetUncollectedOrders.Text = "GetUncollectedOrders"
        Me.BtGetUncollectedOrders.UseVisualStyleBackColor = True
        '
        'BtSetOrderStatus
        '
        Me.BtSetOrderStatus.Location = New System.Drawing.Point(6, 187)
        Me.BtSetOrderStatus.Name = "BtSetOrderStatus"
        Me.BtSetOrderStatus.Size = New System.Drawing.Size(280, 31)
        Me.BtSetOrderStatus.TabIndex = 11
        Me.BtSetOrderStatus.Text = "SetOrderStatus"
        Me.BtSetOrderStatus.UseVisualStyleBackColor = True
        '
        'BtSetOrderStatusByRefno
        '
        Me.BtSetOrderStatusByRefno.Location = New System.Drawing.Point(6, 261)
        Me.BtSetOrderStatusByRefno.Name = "BtSetOrderStatusByRefno"
        Me.BtSetOrderStatusByRefno.Size = New System.Drawing.Size(280, 31)
        Me.BtSetOrderStatusByRefno.TabIndex = 12
        Me.BtSetOrderStatusByRefno.Text = "SetOrderStatusByRefno"
        Me.BtSetOrderStatusByRefno.UseVisualStyleBackColor = True
        '
        'BtSetOrderStatusWithETA
        '
        Me.BtSetOrderStatusWithETA.Location = New System.Drawing.Point(342, 335)
        Me.BtSetOrderStatusWithETA.Name = "BtSetOrderStatusWithETA"
        Me.BtSetOrderStatusWithETA.Size = New System.Drawing.Size(280, 31)
        Me.BtSetOrderStatusWithETA.TabIndex = 13
        Me.BtSetOrderStatusWithETA.Text = "SetOrderStatusWithETA"
        Me.BtSetOrderStatusWithETA.UseVisualStyleBackColor = True
        '
        'BtSetOrderToInterventionStatus
        '
        Me.BtSetOrderToInterventionStatus.Location = New System.Drawing.Point(6, 224)
        Me.BtSetOrderToInterventionStatus.Name = "BtSetOrderToInterventionStatus"
        Me.BtSetOrderToInterventionStatus.Size = New System.Drawing.Size(280, 31)
        Me.BtSetOrderToInterventionStatus.TabIndex = 14
        Me.BtSetOrderToInterventionStatus.Text = "SetOrderToInterventionStatus"
        Me.BtSetOrderToInterventionStatus.UseVisualStyleBackColor = True
        '
        'BtSetOrderToInterventionStatusByRefNo
        '
        Me.BtSetOrderToInterventionStatusByRefNo.Location = New System.Drawing.Point(6, 298)
        Me.BtSetOrderToInterventionStatusByRefNo.Name = "BtSetOrderToInterventionStatusByRefNo"
        Me.BtSetOrderToInterventionStatusByRefNo.Size = New System.Drawing.Size(280, 31)
        Me.BtSetOrderToInterventionStatusByRefNo.TabIndex = 15
        Me.BtSetOrderToInterventionStatusByRefNo.Text = "SetOrderToInterventionStatusByRefNo"
        Me.BtSetOrderToInterventionStatusByRefNo.UseVisualStyleBackColor = True
        '
        'BtSetStatusCredit
        '
        Me.BtSetStatusCredit.Location = New System.Drawing.Point(6, 335)
        Me.BtSetStatusCredit.Name = "BtSetStatusCredit"
        Me.BtSetStatusCredit.Size = New System.Drawing.Size(280, 31)
        Me.BtSetStatusCredit.TabIndex = 16
        Me.BtSetStatusCredit.Text = "SetStatusCredit"
        Me.BtSetStatusCredit.UseVisualStyleBackColor = True
        '
        'Txtmessage
        '
        Me.Txtmessage.Location = New System.Drawing.Point(6, 21)
        Me.Txtmessage.Multiline = True
        Me.Txtmessage.Name = "Txtmessage"
        Me.Txtmessage.Size = New System.Drawing.Size(616, 125)
        Me.Txtmessage.TabIndex = 0
        '
        'BtGetOrderIDByRefno
        '
        Me.BtGetOrderIDByRefno.Location = New System.Drawing.Point(342, 224)
        Me.BtGetOrderIDByRefno.Name = "BtGetOrderIDByRefno"
        Me.BtGetOrderIDByRefno.Size = New System.Drawing.Size(280, 31)
        Me.BtGetOrderIDByRefno.TabIndex = 17
        Me.BtGetOrderIDByRefno.Text = "GetOrderIDByRefno"
        Me.BtGetOrderIDByRefno.UseVisualStyleBackColor = True
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(652, 571)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Txtpassword As TextBox
    Friend WithEvents Txtuser As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents BtGetOMAByOrderID As Button
    Friend WithEvents BtSetStatusCredit As Button
    Friend WithEvents BtSetOrderToInterventionStatusByRefNo As Button
    Friend WithEvents BtSetOrderToInterventionStatus As Button
    Friend WithEvents BtSetOrderStatusWithETA As Button
    Friend WithEvents BtSetOrderStatusByRefno As Button
    Friend WithEvents BtSetOrderStatus As Button
    Friend WithEvents BtGetUncollectedOrders As Button
    Friend WithEvents BtGetOrderIDs As Button
    Friend WithEvents BtGetOrderIdentifiersByCustomerOrderNo As Button
    Friend WithEvents BtGetOrderDefinitions As Button
    Friend WithEvents BtGetOrderDefinition As Button
    Friend WithEvents BtGetOMAByRefNo As Button
    Friend WithEvents Txtmessage As TextBox
    Friend WithEvents BtGetOrderIDByRefno As Button
End Class
