﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DataListPara = New System.Windows.Forms.DataGridView()
        Me.DPkerestimatedDeliveredDate = New System.Windows.Forms.DateTimePicker()
        Me.DPkerupdateTime = New System.Windows.Forms.DateTimePicker()
        Me.Txtstatus = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.TxtsupplyJobNumber = New System.Windows.Forms.TextBox()
        Me.TxtfileNumber = New System.Windows.Forms.TextBox()
        Me.Txtsociety = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.Txtpassword = New System.Windows.Forms.TextBox()
        Me.BtCallApi = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Txtuser = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Txturl = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btsaveresult = New System.Windows.Forms.Button()
        Me.TxtResult = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.DataListPara, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox4)
        Me.GroupBox1.Controls.Add(Me.GroupBox3)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 10)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(921, 319)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Call"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Button2)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.Button1)
        Me.GroupBox4.Controls.Add(Me.DataListPara)
        Me.GroupBox4.Controls.Add(Me.DPkerestimatedDeliveredDate)
        Me.GroupBox4.Controls.Add(Me.DPkerupdateTime)
        Me.GroupBox4.Controls.Add(Me.Txtstatus)
        Me.GroupBox4.Controls.Add(Me.Label7)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.Label9)
        Me.GroupBox4.Controls.Add(Me.TxtsupplyJobNumber)
        Me.GroupBox4.Controls.Add(Me.TxtfileNumber)
        Me.GroupBox4.Controls.Add(Me.Txtsociety)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label5)
        Me.GroupBox4.Controls.Add(Me.Label4)
        Me.GroupBox4.Location = New System.Drawing.Point(379, 20)
        Me.GroupBox4.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox4.Size = New System.Drawing.Size(535, 295)
        Me.GroupBox4.TabIndex = 17
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Parameter"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(414, 151)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(84, 40)
        Me.Button2.TabIndex = 17
        Me.Button2.Text = "Remove list parameter"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(5, 135)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(73, 13)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "List parameter"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(414, 41)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(84, 34)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Add parameter to list"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DataListPara
        '
        Me.DataListPara.AllowUserToOrderColumns = True
        Me.DataListPara.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataListPara.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DataListPara.Location = New System.Drawing.Point(8, 151)
        Me.DataListPara.Margin = New System.Windows.Forms.Padding(2)
        Me.DataListPara.Name = "DataListPara"
        Me.DataListPara.RowTemplate.Height = 24
        Me.DataListPara.Size = New System.Drawing.Size(390, 139)
        Me.DataListPara.TabIndex = 16
        '
        'DPkerestimatedDeliveredDate
        '
        Me.DPkerestimatedDeliveredDate.CustomFormat = "dd-MM-yyyy"
        Me.DPkerestimatedDeliveredDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkerestimatedDeliveredDate.Location = New System.Drawing.Point(214, 115)
        Me.DPkerestimatedDeliveredDate.Margin = New System.Windows.Forms.Padding(2)
        Me.DPkerestimatedDeliveredDate.Name = "DPkerestimatedDeliveredDate"
        Me.DPkerestimatedDeliveredDate.Size = New System.Drawing.Size(185, 20)
        Me.DPkerestimatedDeliveredDate.TabIndex = 13
        '
        'DPkerupdateTime
        '
        Me.DPkerupdateTime.CustomFormat = "dd-MM-yyyy H:mm:ss"
        Me.DPkerupdateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DPkerupdateTime.Location = New System.Drawing.Point(214, 78)
        Me.DPkerupdateTime.Margin = New System.Windows.Forms.Padding(2)
        Me.DPkerupdateTime.Name = "DPkerupdateTime"
        Me.DPkerupdateTime.Size = New System.Drawing.Size(185, 20)
        Me.DPkerupdateTime.TabIndex = 12
        '
        'Txtstatus
        '
        Me.Txtstatus.Location = New System.Drawing.Point(214, 41)
        Me.Txtstatus.Margin = New System.Windows.Forms.Padding(2)
        Me.Txtstatus.Name = "Txtstatus"
        Me.Txtstatus.Size = New System.Drawing.Size(185, 20)
        Me.Txtstatus.TabIndex = 9
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(212, 98)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(120, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "estimatedDeliveredDate"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(212, 62)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(63, 13)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "updateTime"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(212, 25)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 13)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "status"
        '
        'TxtsupplyJobNumber
        '
        Me.TxtsupplyJobNumber.Location = New System.Drawing.Point(8, 115)
        Me.TxtsupplyJobNumber.Margin = New System.Windows.Forms.Padding(2)
        Me.TxtsupplyJobNumber.Name = "TxtsupplyJobNumber"
        Me.TxtsupplyJobNumber.Size = New System.Drawing.Size(185, 20)
        Me.TxtsupplyJobNumber.TabIndex = 5
        '
        'TxtfileNumber
        '
        Me.TxtfileNumber.Location = New System.Drawing.Point(8, 78)
        Me.TxtfileNumber.Margin = New System.Windows.Forms.Padding(2)
        Me.TxtfileNumber.Name = "TxtfileNumber"
        Me.TxtfileNumber.Size = New System.Drawing.Size(185, 20)
        Me.TxtfileNumber.TabIndex = 4
        '
        'Txtsociety
        '
        Me.Txtsociety.Location = New System.Drawing.Point(8, 41)
        Me.Txtsociety.Margin = New System.Windows.Forms.Padding(2)
        Me.Txtsociety.Name = "Txtsociety"
        Me.Txtsociety.Size = New System.Drawing.Size(185, 20)
        Me.Txtsociety.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(5, 98)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 13)
        Me.Label6.TabIndex = 2
        Me.Label6.Text = "supplyJobNumber"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(5, 62)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(57, 13)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "fileNumber"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 25)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(40, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "society"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Txtpassword)
        Me.GroupBox3.Controls.Add(Me.BtCallApi)
        Me.GroupBox3.Controls.Add(Me.Label1)
        Me.GroupBox3.Controls.Add(Me.Txtuser)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.Txturl)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Location = New System.Drawing.Point(4, 20)
        Me.GroupBox3.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox3.Size = New System.Drawing.Size(370, 295)
        Me.GroupBox3.TabIndex = 16
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "HttpRequest"
        '
        'Txtpassword
        '
        Me.Txtpassword.Location = New System.Drawing.Point(7, 115)
        Me.Txtpassword.Margin = New System.Windows.Forms.Padding(2)
        Me.Txtpassword.Name = "Txtpassword"
        Me.Txtpassword.Size = New System.Drawing.Size(356, 20)
        Me.Txtpassword.TabIndex = 20
        '
        'BtCallApi
        '
        Me.BtCallApi.Location = New System.Drawing.Point(164, 171)
        Me.BtCallApi.Margin = New System.Windows.Forms.Padding(2)
        Me.BtCallApi.Name = "BtCallApi"
        Me.BtCallApi.Size = New System.Drawing.Size(201, 119)
        Me.BtCallApi.TabIndex = 15
        Me.BtCallApi.Text = "Call Api"
        Me.BtCallApi.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 98)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(70, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "PASSWORD"
        '
        'Txtuser
        '
        Me.Txtuser.Location = New System.Drawing.Point(7, 78)
        Me.Txtuser.Margin = New System.Windows.Forms.Padding(2)
        Me.Txtuser.Name = "Txtuser"
        Me.Txtuser.Size = New System.Drawing.Size(356, 20)
        Me.Txtuser.TabIndex = 18
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 62)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "USER NAME"
        '
        'Txturl
        '
        Me.Txturl.Location = New System.Drawing.Point(7, 41)
        Me.Txturl.Margin = New System.Windows.Forms.Padding(2)
        Me.Txturl.Name = "Txturl"
        Me.Txturl.Size = New System.Drawing.Size(356, 20)
        Me.Txturl.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(4, 25)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(61, 13)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "PATH URL"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TxtResult)
        Me.GroupBox2.Controls.Add(Me.btsaveresult)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 334)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox2.Size = New System.Drawing.Size(921, 238)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Result"
        '
        'btsaveresult
        '
        Me.btsaveresult.Location = New System.Drawing.Point(793, 18)
        Me.btsaveresult.Margin = New System.Windows.Forms.Padding(2)
        Me.btsaveresult.Name = "btsaveresult"
        Me.btsaveresult.Size = New System.Drawing.Size(84, 50)
        Me.btsaveresult.TabIndex = 1
        Me.btsaveresult.Text = "Save Result To Database"
        Me.btsaveresult.UseVisualStyleBackColor = True
        '
        'TxtResult
        '
        Me.TxtResult.Location = New System.Drawing.Point(5, 18)
        Me.TxtResult.Multiline = True
        Me.TxtResult.Name = "TxtResult"
        Me.TxtResult.Size = New System.Drawing.Size(772, 215)
        Me.TxtResult.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(938, 582)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Novacel"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.DataListPara, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents BtCallApi As Button
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents Txtpassword As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Txtuser As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Txturl As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents DPkerupdateTime As DateTimePicker
    Friend WithEvents Txtstatus As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents TxtsupplyJobNumber As TextBox
    Friend WithEvents TxtfileNumber As TextBox
    Friend WithEvents Txtsociety As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents DPkerestimatedDeliveredDate As DateTimePicker
    Friend WithEvents DataListPara As DataGridView
    Friend WithEvents Label10 As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents btsaveresult As Button
    Friend WithEvents TxtResult As TextBox
End Class
