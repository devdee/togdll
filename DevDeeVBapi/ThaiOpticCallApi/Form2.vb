﻿Imports ThaiOpticCallApi.thaioptic

Public Class Form2
    Dim username As String = "TOG"
    Dim password As String = "T0d4esf6kDk40pd!"

    Private Sub BtGetOMAByOrderID_Click(sender As Object, e As EventArgs) Handles BtGetOMAByOrderID.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            Dim orderId As Guid = Guid.NewGuid()
            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim objResult As New OMARequestResponse()
            objResult = client.GetOMAByOrderID(yzoCredential, orderId)
            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetOrderIDs_Click(sender As Object, e As EventArgs) Handles BtGetOrderIDs.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim testOrders As Boolean
            Dim lastOrderNo As Long
            Dim amountOfOrders As Integer

            Dim objResult As New OrderIDResponse()

            objResult = client.GetOrderIDs(testOrders, yzoCredential, lastOrderNo, amountOfOrders)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetUncollectedOrders_Click(sender As Object, e As EventArgs) Handles BtGetUncollectedOrders.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim testOrders As Boolean
            Dim amountOfOrders As Integer

            Dim objResult As New OrderIDResponse()

            objResult = client.GetUncollectedOrders(testOrders, yzoCredential, amountOfOrders)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetOrderDefinition_Click(sender As Object, e As EventArgs) Handles BtGetOrderDefinition.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim orderId As Guid = Guid.NewGuid()
            Dim objResult As New OrderDefinitionResponse()

            objResult = client.GetOrderDefinition(yzoCredential, orderId)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetOrderDefinitions_Click(sender As Object, e As EventArgs) Handles BtGetOrderDefinitions.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim orderIDList As New List(Of Guid)
            Dim objResult As New OrderDefinitionArrayResponse()

            objResult = client.GetOrderDefinitions(yzoCredential, orderIDList)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtSetOrderStatus_Click(sender As Object, e As EventArgs) Handles BtSetOrderStatus.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim OrderID As Guid
            Dim Status As New OrderStatuses
            Dim objResult As New Response()

            objResult = client.SetOrderStatus(yzoCredential, OrderID, Status)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtSetOrderToInterventionStatus_Click(sender As Object, e As EventArgs) Handles BtSetOrderToInterventionStatus.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim OrderID As Guid
            Dim labNotes As String
            Dim objResult As New Response()

            objResult = client.SetOrderToInterventionStatus(yzoCredential, OrderID, labNotes)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetOrderIDByRefno_Click(sender As Object, e As EventArgs) Handles BtGetOrderIDByRefno.Click
        Try

            Dim Refno As Long

            Txtmessage.Text = Refno.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtSetOrderStatusByRefno_Click(sender As Object, e As EventArgs) Handles BtSetOrderStatusByRefno.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim refNo As Long
            Dim Status As New OrderStatuses
            Dim objResult As New Response()

            objResult = client.SetOrderStatusByRefno(yzoCredential, refNo, Status)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetOMAByRefNo_Click(sender As Object, e As EventArgs) Handles BtGetOMAByRefNo.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim refNo As Long
            Dim objResult As New OMARequestResponse()

            objResult = client.GetOMAByRefNo(yzoCredential, refNo)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtSetOrderToInterventionStatusByRefNo_Click(sender As Object, e As EventArgs) Handles BtSetOrderToInterventionStatusByRefNo.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim refNo As Long
            Dim labNotes As String
            Dim objResult As New Response()

            objResult = client.SetOrderStatusByRefno(yzoCredential, refNo, labNotes)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtGetOrderIdentifiersByCustomerOrderNo_Click(sender As Object, e As EventArgs) Handles BtGetOrderIdentifiersByCustomerOrderNo.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim CustomerOrderNo As String
            Dim objResult As New OrderIdentifiers()

            objResult = client.GetOrderIdentifiersByCustomerOrderNo(yzoCredential, CustomerOrderNo)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtSetStatusCredit_Click(sender As Object, e As EventArgs) Handles BtSetStatusCredit.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim creditRequestId As String
            Dim Status As New CreditRequestStatus
            Dim Message As String
            Dim amount As Decimal
            Dim objResult As New Response()

            objResult = client.SetStatusCredit(yzoCredential, creditRequestId, Status, Message, amount)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BtSetOrderStatusWithETA_Click(sender As Object, e As EventArgs) Handles BtSetOrderStatusWithETA.Click
        Try
            Dim client As LabsServiceSoapClient = New LabsServiceSoapClient()
            Dim yzoCredential As LoginCredentials = New LoginCredentials()

            yzoCredential.Username = username
            yzoCredential.Password = password

            Dim OrderID As Guid
            Dim creditRequestId As String
            Dim Status As New OrderStatuses
            Dim Eta As New System.Nullable(Of Date)
            Dim objResult As New Response()

            objResult = client.SetOrderStatusWithETA(yzoCredential, OrderID, Status, Eta)

            Txtmessage.Text = objResult.Message.ToString
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class