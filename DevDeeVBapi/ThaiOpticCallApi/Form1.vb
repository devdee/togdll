﻿Imports System.Data.SqlClient
Imports DevDee.ApiHelper
Imports Newtonsoft.Json

Public Class Form1

    'Dim pathUrl As String = "http://nttrkspp.novacel-optical.com/WebService/SRV_trackingSupply.php"
    'Dim username As String = "wsTOG"
    'Dim password As String = "C01dq1o-3zjf"
    Dim pathUrl As String '= "http://nttrkspp.novacel-optical.com/WebService/SRV_trackingSupply.php"
    Dim username As String
    Dim password As String
    Dim basicauth As Boolean = True
    Dim jsonResult As String
    Dim resultdata As nResult
    Dim datamodel As New ApiHelper.datajson()
    Dim dataempty As New ApiHelper.datajson()
    'Dim resultdata As New ApiHelper.nResult()
    Public Class nResult
        Public code As String
        Public message As String
        Public datetime As String
        Public description As String
        Public data As List(Of dtResult)
    End Class
    Public Class dtResult
        Public code As Integer
        Public message As String
        Public datetime As DateTime
        Public description As String
        Public data As String
    End Class
    Public Class datatext
        Public society As String
        Public fileNumber As String
        Public supplyJobNumber As String
        Public status As String
        Public updateTime As String
        Public estimatedDeliveredDate As String
    End Class

    Public Class datajson
        Public jsonFlow As datatext
    End Class

    Private Sub BtCallApi_Click(sender As Object, e As EventArgs) Handles BtCallApi.Click
            BtCallApi.Enabled = False
            If Txturl.Text = "" Or Txtuser.Text = "" Or Txtpassword.Text = "" Then
                If Txturl.Text = "" Then
                    MessageBox.Show("Please input 'PATH URL' Field")
                End If
                If Txtuser.Text = "" Then
                    MessageBox.Show("Please input 'USER NAME' Field")
                End If
                If Txtpassword.Text = "" Then
                    MessageBox.Show("Please input 'PASSWORD' Field")
                End If
            Else
            pathUrl = Txturl.Text
            username = Txtuser.Text
            password = Txtpassword.Text
            'basicauth = ChkBasicAuth.Checked
            'Dim dt As New Library.datatext()
            'dt.society = "TOG"
            'dt.fileNumber = "1000012"
            'dt.supplyJobNumber = "9876543210"
            'dt.status = "WIP"
            'dt.updateTime = "2019-09-24 10:00:00"
            'dt.estimatedDeliveredDate = "2019-09-20"

            'datamodel.jsonFlow.Add(dt)
            'datamodel.jsonFlow.Add(dt)
            'datamodel.jsonFlow.Add(dt)
            'datamodel.jsonFlow.Add(dt)

            Dim result As nResult
                Dim callapi As New ApiHelper()
            jsonResult = callapi.CallApi(pathUrl, datamodel, username, password, basicauth)
            TxtResult.Text = jsonResult
            result = JsonConvert.DeserializeObject(Of nResult)(jsonResult)
                If result.code = 200 And result.data IsNot Nothing Then
                    'If result.data = 0 Then
                    '    MessageBox.Show("No Information")
                    'Else
                    resultdata = result
                'DataResult.DataSource = resultdata.data


                'End If
            Else
                    MessageBox.Show(result.description)
                End If
            End If
            BtCallApi.Enabled = True

        End Sub

        Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
            Dim dt As New ApiHelper.datatext()

            dt.society = Txtsociety.Text
            dt.fileNumber = TxtfileNumber.Text
            dt.supplyJobNumber = TxtsupplyJobNumber.Text
            dt.status = Txtstatus.Text
            dt.updateTime = DPkerupdateTime.Value.ToString("yyyy-MM-dd H:mm:ss")
            dt.estimatedDeliveredDate = DPkerestimatedDeliveredDate.Value.ToString("yyyy-MM-dd")

            datamodel.jsonFlow.Add(dt)
            DataListPara.DataSource = dataempty.jsonFlow
            DataListPara.DataSource = datamodel.jsonFlow
        End Sub

        Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
            datamodel.jsonFlow.Clear()
            DataListPara.DataSource = dataempty.jsonFlow

        End Sub

        Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btsaveresult.Click
            btsaveresult.Enabled = False
            Try
                Dim connectionString As String
                connectionString = GetConnectionString()
                Dim connection As New SqlConnection(connectionString)
                Dim SqlCommand As New SqlCommand("INSERT INTO t_thaioptic ( code, message , datetime, description, data) VALUES(@code, @message, @datetime,  @description, @data ) ", connection)

                connection.Open()

            Dim temp As New dtResult
            SqlCommand.Parameters.AddWithValue("@code", temp.code)
                SqlCommand.Parameters.AddWithValue("@message", temp.message)
                SqlCommand.Parameters.AddWithValue("@datetime", temp.datetime)
                SqlCommand.Parameters.AddWithValue("@description", temp.description)
                SqlCommand.Parameters.AddWithValue("@data", temp.data)

            For Each item As dtResult In resultdata.data
                SqlCommand.Parameters("@code").Value = item.code
                SqlCommand.Parameters("@message").Value = item.message
                SqlCommand.Parameters("@datetime").Value = item.datetime
                SqlCommand.Parameters("@description").Value = item.description
                SqlCommand.Parameters("@data").Value = item.data
                SqlCommand.ExecuteNonQuery()
            Next

            connection.Close()

                MessageBox.Show("success")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
            btsaveresult.Enabled = True
        End Sub
        Public Function GetConnectionString() As String
            Dim DBConnection As String
            DBConnection = System.Configuration.ConfigurationManager.ConnectionStrings("ThaiOpticDB").ToString()
            Return DBConnection
        End Function
    End Class
